package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.model.ERole;
import be.greenteam.lacuisine.model.Role;
import be.greenteam.lacuisine.model.User;
import be.greenteam.lacuisine.security.WebSecurityConfig;
import be.greenteam.lacuisine.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class, excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = WebSecurityConfig.class)})
class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;
    private List<User> userList;
    private Set<Role> roleList;
    private Role role = new Role(1L, ERole.ROLE_USER);

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        this.userList = new ArrayList<>();
        this.roleList = new HashSet<>();
        this.roleList.add(this.role);
        this.userList.add(new User(1L, "testUser1", "testEmail.com", "testWW", roleList,true));
        this.userList.add(new User(2L, "testUser2", "testEmail.com", "testWW", roleList,true));
        this.userList.add(new User(3L, "testUser3", "testEmail.com", "testWW", roleList,true));
    }

    @Test
    @WithMockUser(username = "john", roles = {"ADMIN"})
    void findUserById() throws Exception {
        User user = new User(1L, "testUser1", "testEmail.com", "testWW", roleList,true);
        when(userService.findById(user.getId())).thenReturn(user);
        //when
        this.mockMvc.perform(get("/api/user/1"))
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.size()",is(userList.size())));
                .andExpect(jsonPath("$.username", is("testUser1")));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void findAllUsers() throws Exception {
        //given
        given(userService.findAll()).willReturn(userList);
        //when
        this.mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.size()",is(userList.size())));
                .andExpect(jsonPath("$.userCollection.size()", is(3)));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteUserById() throws Exception {
//       Long userId = 1L;
//       User user = new User(userId, "testUser1", "testEmail.com", "testWW", roleList);
//          given(userService.findAll()).willReturn(userList);
//             doNothing().when(userService).deleteById(1L);
////        //this.mockMvc.perform(get("/api/user"));
//        this.mockMvc.perform(delete("/api/user/{id}",1L))
//                .andExpect(status().isOk());

        //# Setup stubs
        doNothing().when(userService).deleteById(1L);
        when(userService.existsById(1L)).thenReturn(true);

        //# Execute controller method
        this.mockMvc.perform(delete("/api/user/{id}",1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //# Check if stubs have been used
        verify(userService).deleteById(1L);
    }
}
