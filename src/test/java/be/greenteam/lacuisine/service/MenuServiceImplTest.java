package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Menu;
import be.greenteam.lacuisine.model.Restaurant;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MenuServiceImplTest {
    @Autowired
    MenuService menuService;
    private Restaurant restaurant;
    private Menu menu1;
    private Menu menu2;
    private Menu menu3;
    private Menu menu4;
    final String name = "Grote friet";

    @BeforeEach
    void setUp() {
        restaurant = new Restaurant();
        menu1 = Menu.builder().name("menu1").restaurant(restaurant).build();
        menu2 = Menu.builder().name("menu2").restaurant(restaurant).build();
        menu3 = Menu.builder().name("menu3").restaurant(restaurant).build();
        menu4 = Menu.builder().name("menu4").restaurant(restaurant).build();
    }

    @Test
    void save() {

        Menu save = menuService.save(menu1);
        menuService.findAll().forEach(System.out::println);
        Assertions.assertEquals(menu1, menuService.findById(save.getId()));
    }

    @Test
    void saveAll() {
    }

    @Test
    void findById() {
    }

    @Test
    void findAll() {
        Assertions.assertEquals(11, menuService.findAll().size());
    }

    @Test
    void existsById() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteAll() {
    }
}
