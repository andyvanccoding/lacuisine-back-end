package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.repository.WalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
class WalletServiceImplTest {
    @Autowired
    private WalletService walletService;
    @Autowired
    private WalletRepository walletRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void addingToStoreCreditById() {
        System.out.println(walletRepository.findValueStoreCredit(3L));
        assertEquals(walletRepository.findValueStoreCredit(3L) + 200,walletService.addingToStoreCreditById(200, 3L) );
        System.out.println(walletRepository.findValueStoreCredit(3L));
        assertEquals(walletRepository.findValueStoreCredit(3L) + 200,walletService.addingToStoreCreditById(200, 3L) );
        System.out.println(walletRepository.findValueStoreCredit(3L));
    }
}