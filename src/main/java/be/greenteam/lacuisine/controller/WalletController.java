package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.exceptions.WalletInsufficientFundException;
import be.greenteam.lacuisine.model.Wallet;
import be.greenteam.lacuisine.payload.collection.WalletCollection;
import be.greenteam.lacuisine.security.services.UserDetailsImpl;
import be.greenteam.lacuisine.service.WalletService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/wallet")
@Slf4j
@RequiredArgsConstructor
public class WalletController {

    private final WalletService walletService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Wallet> findWalletById(@PathVariable Long id) {
        return ResponseEntity.ok(walletService.findById(id));
    }

    @GetMapping()
    public ResponseEntity<WalletCollection> findAllWallets() {
        return ResponseEntity.ok(WalletCollection.builder().walletCollection(walletService.findAll()).build());
    }

    @PutMapping("/add")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> addToWallet(@RequestParam("value") float value, @RequestParam("id") Long id) {

        if (id == null || !walletService.existsById(id)) {
            throw new NotFoundException("Wallet could not be found for id: " + id);
        }

        Wallet wallet = walletService.findById(id);
        if (wallet.getStoreCredit() + value < 0) {
            throw new WalletInsufficientFundException("Insufficient funds on wallet with id: " + id);
        }
        UserDetailsImpl userDetails =
                (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        log.debug("Getting id from principal: " + userDetails.getId());

        return ResponseEntity.ok(walletService.addingToStoreCreditById(value, id));
    }

}
