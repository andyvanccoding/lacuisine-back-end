package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Order;
import be.greenteam.lacuisine.payload.collection.OrderCollection;
import be.greenteam.lacuisine.payload.collection.OrderItemCollection;
import be.greenteam.lacuisine.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/order")
@Slf4j
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Order> findOrderById(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.findById(id));
    }

    @GetMapping()
    public ResponseEntity<OrderCollection> findAllOrders(
            @RequestParam(value = "restaurantId", required = false) Long restaurantId,
            @RequestParam(value = "userId", required = false) Long userId) {
        if(restaurantId != null){
            return ResponseEntity.ok(OrderCollection.builder().orderCollection(orderService.findAllByRestaurantIdOrderByOrderTimeDesc(restaurantId)).build());
        }
        if(userId != null){
            return ResponseEntity.ok(OrderCollection.builder().orderCollection(orderService.findAllByUserIdOrderByOrderTimeDesc(userId)).build());
        }
        return ResponseEntity.ok(OrderCollection.builder().orderCollection(orderService.findAll()).build());
    }

    @PostMapping()
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Order> saveOrder(@RequestBody Order order) {
//        if (order.getId() != null) {
//            throw new AllreadyExistException("New Order should not have an id. Recieved id: " + order.getId());
//        }
        log.debug("order to save: " + order.getId());
        order.setOrderTime(LocalDateTime.now());
        Order saveOrder = orderService.save(order);
        return ResponseEntity.ok(saveOrder);
    }

    @PutMapping
    public ResponseEntity<?> updateOrder(@RequestBody Order order) {
        if (order.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (order.getId() != null && !orderService.existsById(order.getId())) {
            throw new NotFoundException(("No Order exists for id: " + order.getId()));
        }
        log.debug("order to update: " + order.getId());
        return ResponseEntity.ok(orderService.save(order));
    }

    @DeleteMapping("/{id:^\\d+$}")
    public ResponseEntity<?> deleteOrderById(@PathVariable Long id) {
        if (!orderService.existsById(id)) {
            throw new NotFoundException("No Order exists for id: " + id);
        }
        orderService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
