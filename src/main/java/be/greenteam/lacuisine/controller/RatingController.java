package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Rating;
import be.greenteam.lacuisine.payload.collection.MenuItemCollection;
import be.greenteam.lacuisine.payload.collection.RatingCollection;
import be.greenteam.lacuisine.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

/**
 * Created by "Dorien en Nick" on 19/08/2021.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/rating")
@Slf4j
@RequiredArgsConstructor
public class RatingController {

    private final RatingService ratingService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Rating> findRatingById(@PathVariable Long id){
        return ResponseEntity.ok(ratingService.findById(id));
    }

    @GetMapping
    public ResponseEntity<RatingCollection> findAllRatingsByRestaurantId(
            @RequestParam(value = "restaurantId", required = false) Long restaurantId,
            @RequestParam(value = "userId", required = false) Long userId){
        if(restaurantId != null){
            return ResponseEntity.ok(RatingCollection.builder().ratingCollection(ratingService.findAllByRestaurantId(restaurantId)).build());
        }
        if(userId != null){
            return ResponseEntity.ok(RatingCollection.builder().ratingCollection(ratingService.findAllByUserId(userId)).build());
        }

        return ResponseEntity.ok(RatingCollection.builder().ratingCollection(ratingService.findAll()).build());
    }

    @PostMapping
    public ResponseEntity<Rating> saveRating(@RequestBody Rating rating){
        if(rating.getId() != null){
            throw new AllreadyExistException("New rating should not have an id. Received id: " + rating.getId());
        }
        log.debug("rating to save: " + rating.getId());
        rating.setDate(LocalDate.now());
        return ResponseEntity.ok(ratingService.save(rating));
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteRating(@PathVariable Long id){
        if (!ratingService.existsById(id)){
            throw new NotFoundException("No rating exists for id: " + id);
        }
        ratingService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
