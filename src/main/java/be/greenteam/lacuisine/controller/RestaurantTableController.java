package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Restaurant;
import be.greenteam.lacuisine.model.RestaurantTable;
import be.greenteam.lacuisine.payload.collection.RestauranttableCollection;
import be.greenteam.lacuisine.service.RestaurantService;
import be.greenteam.lacuisine.service.RestaurantTableService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by "Andy Van Camp" on 18/08/2021.
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/restauranttable")
@Slf4j
@RequiredArgsConstructor
public class RestaurantTableController {

    private final RestaurantTableService restaurantTableService;
    private final RestaurantService restaurantService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<RestaurantTable> findTableById(@PathVariable Long id) {
        if (!restaurantTableService.existsById(id)) {
            throw new NotFoundException("No Table found for: " + id);
        }
        return ResponseEntity.ok(restaurantTableService.findById(id));
    }

    @GetMapping
    public ResponseEntity<RestauranttableCollection> findAllRestaurantTable(@RequestParam(value = "restaurantId", required = false) Long restaurantId) {
        if (restaurantId != null) {
            if (!restaurantService.existsById(restaurantId)) {
                throw new NotFoundException("No restaurant found with id: " + restaurantId);
            }
            return ResponseEntity.ok(RestauranttableCollection.builder().restaurantTableCollection(restaurantTableService.findAllByRestaurantId(restaurantId)).build());
        }
        return ResponseEntity.ok(RestauranttableCollection.builder().restaurantTableCollection(restaurantTableService.findAll()).build());
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<RestaurantTable> saveRestaurantTable(@RequestBody RestaurantTable restaurantTable) {
        if (restaurantTable.getId() != null) {
            throw new AllreadyExistException("New Table should not have an id. Received id: " + restaurantTable.getId());
        }
        Restaurant foundResto = restaurantService.findById(restaurantTable.getRestaurant().getId());
        restaurantTable.setRestaurant(foundResto);
        return ResponseEntity.ok(restaurantTableService.save(restaurantTable));
    }

    @PutMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<RestaurantTable> updateRestaurantTable(@RequestBody RestaurantTable restaurantTable) {
        if (restaurantTable.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (!restaurantTableService.existsById(restaurantTable.getId())) {
            throw new NotFoundException("No Table exists for id: " + restaurantTable.getId());
        }
        log.debug("restaurant table to update: " + restaurantTable.getCapacity());

        return ResponseEntity.ok(restaurantTableService.save(restaurantTable));
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteRestaurantTableById(@PathVariable Long id) {
        if (!restaurantTableService.existsById(id)) {
            throw new NotFoundException("No Table exists for id: " + id);
        }
        restaurantTableService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
