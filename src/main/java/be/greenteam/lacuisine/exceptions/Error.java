package be.greenteam.lacuisine.exceptions;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@Data
@RequiredArgsConstructor
public class Error extends Throwable {
    private final HttpStatus httpStatus;
    private final String message;
}
