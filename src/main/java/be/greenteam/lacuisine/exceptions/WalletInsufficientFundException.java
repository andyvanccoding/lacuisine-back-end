package be.greenteam.lacuisine.exceptions;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
public class WalletInsufficientFundException extends RuntimeException {

    public WalletInsufficientFundException() {
        super();
    }

    public WalletInsufficientFundException(String message) {
        super(message);
    }

    public WalletInsufficientFundException(String message, Throwable cause) {
        super(message, cause);
    }
}
