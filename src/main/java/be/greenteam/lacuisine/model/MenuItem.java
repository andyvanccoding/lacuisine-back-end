package be.greenteam.lacuisine.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "menu_id")
    private Menu menu;

    private double price;
    @Enumerated(EnumType.STRING)
    @Column(name = "food_type")
    private FoodType foodType;
    private boolean vegetarian;

    private String name;

    public MenuItem(Menu menu, double price, FoodType foodType, boolean vegetarian, String name) {
        this.menu = menu;
        this.price = price;
        this.foodType = foodType;
        this.vegetarian = vegetarian;
        this.name = name;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "id=" + id +
                ", menu=" + menu.getId() +
                ", price=" + price +
                ", foodType=" + foodType +
                ", vegetarian=" + vegetarian +
                ", name='" + name + '\'' +
                '}';
    }



}
