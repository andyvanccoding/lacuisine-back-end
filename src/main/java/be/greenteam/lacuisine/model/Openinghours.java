package be.greenteam.lacuisine.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalTime;

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Openinghours {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private LocalTime openTime;
    @NotNull
    private LocalTime closeTime;
    @NotNull
    private int reservationTimeslot;

    private boolean active;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DayOfWeek weekday;

    @NotNull
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Restaurant restaurant;

    @Override
    public String toString() {
        return "Openinghours{" +
                "id=" + id +
                ", openTime=" + openTime +
                ", closeTime=" + closeTime +
                ", reservationTimeslot=" + reservationTimeslot +
                ", active=" + active +
                ", weekday=" + weekday +
                '}';
    }
}
