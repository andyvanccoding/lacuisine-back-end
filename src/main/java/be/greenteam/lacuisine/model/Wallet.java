package be.greenteam.lacuisine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@Entity
@Getter
@Setter
public class Wallet {

    @Id
    @Column(name = "user_id")
    private Long id;

    private float storeCredit;

    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
}
