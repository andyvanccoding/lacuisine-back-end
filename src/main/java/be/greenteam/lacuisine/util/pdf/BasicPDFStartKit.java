package be.greenteam.lacuisine.util.pdf;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;

import java.io.OutputStream;

/**
 * Created by "Andy Van Camp" on 9/11/2021.
 */

/**
 * This class will hold a document and the output-stream to write the pdf data to.
 */
public abstract class BasicPDFStartKit {

    protected Document document;
    protected OutputStream outputStream;

    /**
     * Create document.
     * @param outputStream
     */
    protected void createPdfDoc(OutputStream outputStream) {
        this.outputStream = outputStream;
        PdfWriter pdfWriter = new PdfWriter(outputStream);
        PdfDocument pdfDoc = new PdfDocument(pdfWriter);
        document = new Document(pdfDoc);
    }

    /**
     * Return the output stream and close document
     *
     * @return OutputStream
     */
    protected OutputStream getPdf() {
        document.close();
        return outputStream;
    }

}
