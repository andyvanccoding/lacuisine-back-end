package be.greenteam.lacuisine.util.zip;

import java.io.*;
import java.util.Set;

/**
 * Created by "Andy Van Camp" on 19/11/2021.
 */
public interface ZipService {
    Integer createZipFile(OutputStream outputStream);

    void addToZip(String zipEntry, ByteArrayInputStream input, Integer key);

    void close(Integer key);

    Set<String> findAllEntriesZipFile(InputStream input) throws IOException;

    ByteArrayOutputStream createNewZipOfEntries(Set entries, InputStream input) throws IOException;

    void writeByteArrayToFile(File file, ByteArrayOutputStream outputStream) throws IOException;

    void extractFilesFromZip(Set entries, InputStream input, File filedirectory) throws IOException;

    void readZipFile(InputStream input) throws IOException;
}
