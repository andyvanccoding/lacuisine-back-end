package be.greenteam.lacuisine.util.reservation.payload.request;

import be.greenteam.lacuisine.model.Restaurant;
import lombok.Data;

import java.time.LocalDate;

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */

@Data
public class TimeslotRequest {

    private LocalDate reservationDate;
    private Restaurant restaurant;
    private int partySize;

}
