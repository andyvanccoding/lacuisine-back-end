package be.greenteam.lacuisine.service;
import be.greenteam.lacuisine.model.Reservation;

import java.time.LocalDate;
import java.util.List;

public interface ReservationService extends CrudService <Reservation, Long>{
    List<Reservation> findAllByUserId(Long id);
    List<Reservation> findAllByRestaurantId(Long id);
    List<Reservation> findAllByRestaurantTableId(Long id);
    List<Reservation> findAllByRestaurantTableIdAndDate(Long id, LocalDate date);

}
