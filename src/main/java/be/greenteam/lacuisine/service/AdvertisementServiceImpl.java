package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Advertisement;
import be.greenteam.lacuisine.repository.AdvertisementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 13/08/2021.
 */
@Service
@RequiredArgsConstructor
public class AdvertisementServiceImpl implements AdvertisementService {

    private final AdvertisementRepository advertisementRepository;

    @Override
    public Advertisement save(Advertisement entity) {
        return advertisementRepository.save(entity);
    }

    @Override
    public List<Advertisement> saveAll(Collection<Advertisement> entities) {
        return advertisementRepository.saveAll(entities);
    }

    @Override
    public Advertisement findById(Long aLong) {
        return advertisementRepository.findById(aLong).orElseThrow(() -> new EntityNotFoundException("No advertisement found with id: " + aLong));
    }

    @Override
    public List<Advertisement> findAll() {
        return advertisementRepository.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return advertisementRepository.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        advertisementRepository.deleteById(aLong);
    }

    @Override
    public void delete(Advertisement entity) {
        advertisementRepository.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Advertisement> entities) {
        advertisementRepository.deleteAll(entities);
    }

    @Override
    public List<Advertisement> findAllByExpireDateBefore(LocalDateTime localDateTime) {
        return advertisementRepository.findAllByExpireDateBefore(localDateTime);
    }
}
