package be.greenteam.lacuisine.service;


import be.greenteam.lacuisine.model.Reservation;
import be.greenteam.lacuisine.repository.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService {
    private final ReservationRepository reservationRepo;

    @Override
    public Reservation save(Reservation entity) {
        return reservationRepo.save(entity);
    }

    @Override
    public List<Reservation> saveAll(Collection<Reservation> entities) {
        return reservationRepo.saveAll(entities);
    }

    @Override
    public Reservation findById(Long aLong) {
        return reservationRepo.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No Reservation found for id:" + aLong)
        );
    }

    @Override
    public List<Reservation> findAll() {
        return reservationRepo.findAll();
    }

    @Override
    public List<Reservation> findAllByRestaurantTableId(Long id) {
        return reservationRepo.findAllByRestaurantTableId(id);
    }

    @Override
    public List<Reservation> findAllByRestaurantTableIdAndDate(Long id, LocalDate date) {
        return reservationRepo.findAllByRestaurantTableIdAndDate(id, date);
    }

    @Override
    public boolean existsById(Long aLong) {
        return reservationRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        reservationRepo.deleteById(aLong);

    }

    @Override
    public void delete(Reservation entity) {
        reservationRepo.delete(entity);

    }

    @Override
    public void deleteAll(Iterable<Reservation> entities) {
        reservationRepo.deleteAll(entities);
    }

    @Override
    public List<Reservation> findAllByUserId(Long id) {
        return reservationRepo.findAllByUserId(id);
    }

    @Override
    public List<Reservation> findAllByRestaurantId(Long id) {
        return reservationRepo.findAllByRestaurantId(id);
    }
}
