package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.ERole;
import be.greenteam.lacuisine.model.Role;

import java.util.Optional;

/**
 * Created by andyv on 23/07/2021.
 */
public interface RoleService extends CrudService<Role, Long>{
    Role findByName(ERole name);
}
