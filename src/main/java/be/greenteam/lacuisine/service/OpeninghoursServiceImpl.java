package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Openinghours;
import be.greenteam.lacuisine.repository.OpeninghoursRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.DayOfWeek;
import java.time.temporal.ChronoField;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */

@Service
@RequiredArgsConstructor
public class OpeninghoursServiceImpl implements OpeninghoursService {

    private final OpeninghoursRepository openinghoursRepository;

    @Override
    public Openinghours save(Openinghours entity) {
        return openinghoursRepository.save(entity);
    }

    @Override
    public List<Openinghours> saveAll(Collection<Openinghours> entities) {
        return openinghoursRepository.saveAll(entities);
    }

    @Override
    public Openinghours findById(Long aLong) {
        return openinghoursRepository.findById(aLong).orElseThrow(() -> new EntityNotFoundException("No entity found for id: " + aLong));
    }

    @Override
    public List<Openinghours> findAll() {
        return openinghoursRepository.findAll();
    }

    @Override
    public List<Openinghours> findAllByRestaurantId(Long id) {
        return openinghoursRepository.findAllByRestaurantId(id).stream()
                .sorted(Comparator.comparingInt(o -> o.getWeekday().get(ChronoField.DAY_OF_WEEK)))
                .collect(Collectors.toList());
    }

    @Override
    public List<Openinghours> findAllByRestaurantIdAndWeekday(Long id, DayOfWeek weekday) {
        return openinghoursRepository.findAllByRestaurantIdAndWeekday(id, weekday);
    }

    @Override
    public boolean existsById(Long aLong) {
        return openinghoursRepository.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        openinghoursRepository.deleteById(aLong);
    }

    @Override
    public void delete(Openinghours entity) {
        openinghoursRepository.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Openinghours> entities) {
        openinghoursRepository.deleteAll(entities);
    }
}
