package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Wallet;
import be.greenteam.lacuisine.repository.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    @Override
    public Wallet save(Wallet entity) {
        return walletRepository.save(entity);
    }

    @Override
    public List<Wallet> saveAll(Collection<Wallet> entities) {
        return walletRepository.saveAll(entities);
    }

    @Override
    public Wallet findById(Long aLong) {
        return walletRepository.findById(aLong).orElseThrow(() -> new EntityNotFoundException("No wallet found with id: " + aLong));
    }

    @Override
    public List<Wallet> findAll() {
        return walletRepository.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return walletRepository.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        walletRepository.deleteById(aLong);
    }

    @Override
    public void delete(Wallet entity) {
        walletRepository.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Wallet> entities) {
        walletRepository.deleteAll(entities);
    }

    @Override
    public float addingToStoreCreditById(float value, Long id) {
        if (walletRepository.existsById(id)) {
            new EntityNotFoundException(("No wallet found with id: " + id));
        }
        walletRepository.addingToStoreCreditById(value, id);
        return walletRepository.findValueStoreCredit(id);
    }
}
