package be.greenteam.lacuisine.service;
/**
 * Author: Andy.
 */
import be.greenteam.lacuisine.model.User;

import java.util.Optional;

public interface UserService extends CrudService<User, Long> {
    User findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
