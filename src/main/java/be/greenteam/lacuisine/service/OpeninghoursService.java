package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Openinghours;

import java.time.DayOfWeek;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */
public interface OpeninghoursService extends CrudService<Openinghours, Long> {
    List<Openinghours> findAllByRestaurantId(Long id);
    List<Openinghours> findAllByRestaurantIdAndWeekday(Long id, DayOfWeek weekday);
}
