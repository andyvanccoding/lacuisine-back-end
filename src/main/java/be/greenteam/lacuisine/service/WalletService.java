package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Wallet;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
public interface WalletService extends CrudService<Wallet, Long> {
    float addingToStoreCreditById(float value, Long id);
}
