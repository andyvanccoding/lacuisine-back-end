package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Address;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
public interface AddressService extends CrudService<Address, Long> {
}
