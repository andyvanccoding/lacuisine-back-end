package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.MenuItem;
import be.greenteam.lacuisine.payload.map.MenuItemMapLine;
import be.greenteam.lacuisine.payload.model.MenuItemData;

import java.util.List;

public interface MenuItemService extends CrudService<MenuItem, Long> {
    List<MenuItem> findAllByMenuId(Long menuId);

    List<MenuItemMapLine> getLineGraphData(Long restaurantId);

    List<MenuItemData> getPieChartGraphData(Long restaurantId, String type);
}
