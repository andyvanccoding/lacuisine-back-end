package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.OrderItem;

import java.util.List;

public interface OrderItemService extends CrudService<OrderItem, Long> {
    List<OrderItem> findOrderItemByOrderId(Long id);
}
