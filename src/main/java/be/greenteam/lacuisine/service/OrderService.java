package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Order;

import java.util.List;

public interface OrderService extends CrudService<Order, Long> {
    List<Order> findAllByRestaurantIdOrderByOrderTimeDesc(Long restaurantId);
    List<Order> findAllByUserIdOrderByOrderTimeDesc(Long userId);
}
