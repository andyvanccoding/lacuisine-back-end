package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Menu;

public interface MenuService extends CrudService<Menu, Long> {
}
