package be.greenteam.lacuisine.bootstrap;

import be.greenteam.lacuisine.model.*;
import be.greenteam.lacuisine.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Component
@Profile({"dev", "prod"})
@RequiredArgsConstructor
@Slf4j
@org.springframework.core.annotation.Order(2)
public class OrderRun implements CommandLineRunner {

    private final RestaurantService restaurantService;
    private final UserService userService;
    private final MenuItemService menuItemService;
    private final OrderItemService orderItemService;
    private final OrderService orderService;


    @Override
    @Transactional
    public void run(String... args) throws Exception {

        Restaurant res4 = restaurantService.findById(4L);
        User user4 = userService.findById(4L);
        MenuItem menuItemFoodOdd = menuItemService.findById(5L);
        MenuItem menuItemFoodEven = menuItemService.findById(8L);
        MenuItem menuItemDrinkOdd = menuItemService.findById(1L);
        MenuItem menuItemDrinkEven = menuItemService.findById(3L);

        for (long i = 1; i < 10; i++) {

            Order order = Order.builder().orderTime(LocalDateTime.now().plusDays(i)).restaurant(res4).user(user4).build();

            Order saveOrder = orderService.save(order);

            OrderItem orderItem = OrderItem.builder().order(saveOrder).amount((int) i).menuItem(i % 2 == 0 ? menuItemFoodEven : menuItemFoodOdd).build();
            OrderItem orderItem1 = OrderItem.builder().order(saveOrder).amount(((int) i)).menuItem(i % 2 == 0 ? menuItemFoodOdd : menuItemFoodEven).build();

            OrderItem orderItem2 = OrderItem.builder().order(saveOrder).amount((int) i).menuItem(i % 2 == 0 ? menuItemDrinkEven : menuItemDrinkOdd).build();
            OrderItem orderItem3 = OrderItem.builder().order(saveOrder).amount(((int) i)).menuItem(i % 2 == 0 ? menuItemDrinkOdd : menuItemDrinkEven).build();


            orderItemService.save(orderItem);
            orderItemService.save(orderItem1);
            orderItemService.save(orderItem2);
            orderItemService.save(orderItem3);

        }
    }
}
