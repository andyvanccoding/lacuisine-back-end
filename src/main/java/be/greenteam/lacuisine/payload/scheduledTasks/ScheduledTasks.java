package be.greenteam.lacuisine.payload.scheduledTasks;

import be.greenteam.lacuisine.service.AdvertisementService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private final AdvertisementService advertisementService;

    //    @Scheduled(fixedRate = 5000)
//    @Scheduled(cron = "*/10 * * * * *")
    @Transactional
    public void reportCurrentTime() {
        log.info("running cron remove add");
        advertisementService.findAllByExpireDateBefore(LocalDateTime.now()).forEach(System.out::println);
        advertisementService.findAllByExpireDateBefore(LocalDateTime.now()).forEach(add -> {
            advertisementService.deleteById(add.getId());
            log.info("Advertisement removed with id: " + add.getId());
        });
    }
}
