package be.greenteam.lacuisine.payload.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MenuItemData {
    private String name;
    private Double value;
}
