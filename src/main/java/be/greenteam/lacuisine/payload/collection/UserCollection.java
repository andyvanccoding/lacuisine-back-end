package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.User;
import lombok.*;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCollection {
    private List<User> userCollection;
}
