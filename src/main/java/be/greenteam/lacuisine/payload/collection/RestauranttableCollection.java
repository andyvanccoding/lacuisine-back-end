package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.RestaurantTable;
import lombok.*;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 18/08/2021.
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestauranttableCollection {
    private List<RestaurantTable> restaurantTableCollection;
}
