package be.greenteam.lacuisine.repository;
/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */

import be.greenteam.lacuisine.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
