package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.RestaurantTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 18/08/2021.
 */
public interface RestaurantTableRepository extends JpaRepository<RestaurantTable, Long> {

    List<RestaurantTable> findAllByRestaurantId(Long id);

    @Query("select t from RestaurantTable t where restaurant_id =:id and capacity >= :capacity ")
    List<RestaurantTable> findAllByRestaurantIdAndCapacity(Long id, int capacity);
}
