package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {
    List<Rating> findAllByRestaurantId(Long restaurantId);
    List<Rating> findAllByUserId(Long userId);
}
